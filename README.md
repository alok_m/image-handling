image-handling  

A Django project that saves images to a server for a certain description.  

localhost:8000/files/modify - form to upload images for a certain description  

localhost:8000/files/dash - frontend queries the database and serves the images in order of **updated_at** .  

The database model has 4 fields -   
* image - ImageField  
* subhead - CharField  
* created_at - DateTimeField  
* updated_at - DateTimeField  
