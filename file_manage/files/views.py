# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, reverse
from .models import Item
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.


def dash(request):
	all_items = Item.objects.all()
	ordered = sorted(all_items, key=(lambda x : x.updated_at), reverse = True)
	return render(request, 'files/dash.html',{'all_items': ordered})

def modify(request):
	all_items = Item.objects.all()
	return render(request, 'files/modify.html',{'all_items': all_items})

def save(request):
	if (request.method == 'POST'):
		newItem = Item.objects.get(subhead = request.POST['item_id'])
		newItem.image = request.FILES['fileToUpload']
		newItem.save()
		return HttpResponse('Description picture changed')
	