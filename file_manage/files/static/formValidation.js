$(document).ready(function () {
	alert("working");

	$("#submitButton").click(function () {
		var input, file;
		var stateFlag = 1;
		var MAX_FILE_SIZE = 512000

		input = document.getElementById('fileToUpload');

		if (!input) {
			alert("Um, couldn't find the fileinput element.");
			stateFlag = 0;
		}
		else if (!input.files) {
			alert("This browser doesn't seem to support the `files` property of file inputs.");
			stateFlage = 0;
		}
		else if (!input.files[0]) {
			alert("Please select a file before clicking 'Upload'");
			stateFlag = 0;
		}
		else {
			file = input.files[0];
			alert("File " + file.name + " is " + file.size + " bytes in size");
			if (file.size > MAX_FILE_SIZE) {
				alert("File must be less than " + MAX_FILE_SIZE / 1024 + " kilobytes");
				stateFlag = 0;

			}
		}
		alert(stateFlag);

        $.ajaxSetup({
            headers: {"X-CSRFToken": getCookie("csrftoken")}
        });
        function getCookie(c_name)
        {
            if (document.cookie.length > 0)
            {
                c_start = document.cookie.indexOf(c_name + "=");
                if (c_start != -1)
                {
                    c_start = c_start + c_name.length + 1;
                    c_end = document.cookie.indexOf(";", c_start);
                    if (c_end == -1) c_end = document.cookie.length;
                    return unescape(document.cookie.substring(c_start,c_end));
                }
            }
            return "";
        }
		if (stateFlag) {
			var form = $("#form");
			var formData = new FormData(form[0]);
			$.ajax({
				type: 'POST',
				url: '/files/save/',
				data: formData,
				success: alert("SUCCESS"),
				cache: false,
				contentType: false,
				processData: false
			});
		}
	});


});