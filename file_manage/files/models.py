# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
def get_directory(instance, filename):
    return '{0}/{1}'.format(instance, filename)
class Item(models.Model):
    image = models.ImageField(upload_to=get_directory)
    subhead = models.CharField(max_length = 100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.subhead