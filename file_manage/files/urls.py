from django.conf.urls import url

#from form import views
from . import views

urlpatterns = [
    url(r'^dash/$',views.dash, name='dash'),
    url(r'^modify/$',views.modify, name='modify'),
    url(r'^save/$',views.save, name='save'),
	]

